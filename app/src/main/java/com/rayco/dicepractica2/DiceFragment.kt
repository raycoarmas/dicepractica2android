package com.rayco.dicepractica2

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_dice.*
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Parcelable

class DiceFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_dice, container, false)
    }

    /**
     * Guarda el ultimo dado mostrado
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if(diceImage.drawable != null && diceImage.drawable is BitmapDrawable){
            val bitmap = (diceImage.drawable as BitmapDrawable).bitmap
            outState.putParcelable(resources.getString(R.string.dice), bitmap)
        }
    }

    /**
     * restaura el ultimo dado mostrado
     */
    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if(savedInstanceState != null) {
            val parcelable: Parcelable? = savedInstanceState.getParcelable(resources.getString(R.string.dice))
            if(parcelable != null && parcelable is Bitmap) {
                diceImage.setImageBitmap(parcelable)
            }
        }
    }

    /**
     * Cambia la imagen del dado
     */
    fun changeDice(dice: Int){
        val resID = resources.getIdentifier(resources.getString(R.string.dice)+dice, "drawable", context!!.packageName)
        diceImage.setImageResource(resID)
    }
}