package com.rayco.dicepractica2


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_select_game_mode.*

class SelectGameModeFragment : Fragment() {

    var mCallback: SelectGameModeCommunicationInterface? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_select_game_mode, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        singlePlayerOnClickListener()
        multiPlayerOnClickListener()
    }

    /**
     * Se obtiene la instancia de la activity para realizar las llamadas callback
     */
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mCallback = context as SelectGameModeCommunicationInterface
    }

    /**
     * Se setea el listener para el el botón de un solo jugador y se llama a la funcion de la activity principal
     */
    private fun singlePlayerOnClickListener(){
        singleplayer.setOnClickListener {
            mCallback!!.singlePlayer()
        }
    }

    /**
     * Se setea el listener para el el botón de dos jugador y se llama a la funcion de la activity principal
     */
    private fun multiPlayerOnClickListener(){
        multiplayers.setOnClickListener {
            mCallback!!.multiPlayer()
        }
    }

    /**
     * se crea una interfaz que implementa la activity para recibir los datos
     * https://developer.android.com/training/basics/fragments/communicating
     */
    interface SelectGameModeCommunicationInterface {
        fun singlePlayer()
        fun multiPlayer()
    }
}
