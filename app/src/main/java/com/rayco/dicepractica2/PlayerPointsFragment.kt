package com.rayco.dicepractica2


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_player_points.*

class PlayerPointsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_player_points, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /* Dependiendo de la String que llegue en el extra se muestra un texto u otro para los jugadores*/
        when (activity!!.intent.getStringExtra(resources.getString(R.string.players))) {
            resources.getString(R.string.singleplayer) -> player2.setText(R.string.machine)
            resources.getString(R.string.multiplayer) -> player2.setText(R.string.player2)
        }
    }

    /**
     * Guarda los puntos actuales al rotar
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(resources.getString(R.string.player1Points), pointsPlayer1.text.toString())
        outState.putString(resources.getString(R.string.player2Points), pointsPlayer2.text.toString())
    }

    /**
     * restaura los puntos despues de rotar
     */
    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if(savedInstanceState != null) {
            pointsPlayer1.text = savedInstanceState.getString(resources.getString(R.string.player1Points))
            progressBarPlayer1.progress = pointsPlayer1.text.toString().toInt()

            pointsPlayer2.text = savedInstanceState.getString(resources.getString(R.string.player2Points))
            progressBarPlayer2.progress = pointsPlayer2.text.toString().toInt()
        }
    }

    /**
     * Recibe los puntos de los jugadores
     */
    fun receivePlayerPoints(accumulated: Int, playerTurn: String?) {
        if (playerTurn.equals(resources.getString(R.string.player1))){
            pointsPlayer1.text = (pointsPlayer1.text.toString().toInt() + accumulated).toString()
            progressBarPlayer1.progress = pointsPlayer1.text.toString().toInt()
        }
        else if (playerTurn.equals(resources.getString(R.string.player2)) || playerTurn.equals(resources.getString(R.string.machine))) {
            pointsPlayer2.text = (pointsPlayer2.text.toString().toInt() + accumulated).toString()
            progressBarPlayer2.progress = pointsPlayer2.text.toString().toInt()
        }
    }

    /**
     * Devulve los puntos del jugador actual
     */
    fun getPlayerPoints(playerTurn: String?): Int {
        var points: Int ? = null
        if (playerTurn.equals(resources.getString(R.string.player1))){
            points =  progressBarPlayer1.progress
        }
        else if (playerTurn.equals(resources.getString(R.string.player2)) || playerTurn.equals(resources.getString(R.string.machine))) {
            points =  progressBarPlayer2.progress
        }
        return points!!
    }

    /**
     * Devuelve verdadero en el caso de tener un jugador ganador
     */
    fun hasWinner(): Boolean {
        return progressBarPlayer1.progress >= resources.getInteger(R.integer.maxPoints) || progressBarPlayer2.progress >= resources.getInteger(R.integer.maxPoints)
    }

    /**
     * Devulve el ganador
     */
    fun winner(): String {
        return if(progressBarPlayer1.progress >= resources.getInteger(R.integer.maxPoints)){
            player1.text.toString()
        }
        else{
            player2.text.toString()
        }
    }

    /**
     * Reinicia las puntuaciones
     */
    fun resetPoints(){
        pointsPlayer1.text = resources.getString(R.string.zeroString)
        progressBarPlayer1.progress = pointsPlayer1.text.toString().toInt()
        pointsPlayer2.text = resources.getString(R.string.zeroString)
        progressBarPlayer2.progress = pointsPlayer2.text.toString().toInt()
    }
}
