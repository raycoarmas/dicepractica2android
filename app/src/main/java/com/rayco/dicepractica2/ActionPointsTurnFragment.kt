package com.rayco.dicepractica2

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.support.v4.app.Fragment
import android.support.v4.content.res.ResourcesCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import kotlinx.android.synthetic.main.fragment_action_points_turn.*
import kotlin.concurrent.thread



class ActionPointsTurnFragment : Fragment() {

    var accumulated: Int? = null
    var mCallback: ActionPointsTurnCommunicationInterface? = null
    private var machineThrows: Int? = null
    var machineThread: Thread? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_action_points_turn, container, false)
    }

    /**
     * Inicialización de los botones y listeners
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        accumulated = resources.getInteger(R.integer.zero)
        turnAccumulated.text = resources.getString(R.string.turnAccumulated,accumulated)
        startTurn?.text = resources.getString(R.string.turnToPlay,resources.getString(R.string.player1))

        throwDiceOnClickListener()
        collectOnClickListener()
        startTurnOnClickLister()
    }

    /**
     * Guarda los puntos y el jugado que le toca jugar al rotar
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        accumulated?.let { outState.putInt(resources.getString(R.string.accumulated), it) }
        machineThrows?.let { outState.putInt(resources.getString(R.string.machineThrows), it) }
        outState.putString(resources.getString(R.string.playerTurn), playerToPlay.text.toString())
        if(machineThread != null && machineThread!!.isAlive){
            machineThread!!.interrupt()
        }
    }

    /**
     * restaura los puntos y el jugado que le toca jugar al rotar
     */
    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if(savedInstanceState != null) {
            accumulated = savedInstanceState.getInt(resources.getString(R.string.accumulated), resources.getInteger(R.integer.zero))
            playerToPlay.text = savedInstanceState.getString(resources.getString(R.string.playerTurn))
            turnAccumulated.text = resources.getString(R.string.turnAccumulated,accumulated)
            machineThrows = savedInstanceState.getInt(resources.getString(R.string.machineThrows), resources.getInteger(R.integer.zero))
            when (playerToPlay.text){
                resources.getString(R.string.player1) -> startTurn?.setTextColor(ResourcesCompat.getColor(resources,R.color.player1Color,null))
                else ->startTurn?.setTextColor(ResourcesCompat.getColor(resources,R.color.player2Color,null))
            }
            startTurn?.text = resources.getString(R.string.turnToPlay,playerToPlay.text )
            turnStartedButtons()

        }
    }

    override fun onResume() {
        super.onResume()
        if (playerToPlay.text == resources.getString(R.string.machine)){
            startMachineTurn(true)
            machineButtonsConf(true)
        }
    }

    /**
     * Se obtiene la instancia de la activity para realizar las llamadas callback
     */
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mCallback = context as ActionPointsTurnCommunicationInterface
    }

    /**
     * Listener para tirar el dado
     */
    private fun throwDiceOnClickListener() {
        throwDice.setOnClickListener {
            collect.visibility = View.VISIBLE
            mCallback!!.onThrowDice(throwDice(), accumulated!!)
            turnAccumulated.text = resources.getString(R.string.turnAccumulated, accumulated)
        }
    }

    /**
     * Listener para recolectar los puntos obtenidos
     */
    private fun collectOnClickListener(){
        collect.setOnClickListener {
            collectPoints()
        }
    }

    /**
     * Listener para empezar el turno
     */
    private fun startTurnOnClickLister(){
        startTurn?.setOnClickListener {
            turnStartedButtons()
        }
    }

    /**
     * Recolecta los puntos acumulados en el turno
     */
    fun collectPoints() {
        val accumulatedFinal = accumulated
        accumulated = resources.getInteger(R.integer.zero)
        turnAccumulated.text = resources.getString(R.string.turnAccumulated, accumulated)
        mCallback!!.collectPoints(accumulatedFinal!!)
    }

    /**
     *  tirada de dados obtiene un valor aleatorio configurador en los contants, en principio 1-6 y devuelve el valor obtenido
     */
    private fun throwDice(): Int {
        val min = resources.getInteger(R.integer.minDice)
        val max = resources.getInteger(R.integer.maxDice)
        val diceThrow = (min..max).random()

        accumulated = when (diceThrow) {
            resources.getInteger(R.integer.autoPassTurn) -> {
                resources.getInteger(R.integer.zero)
            }
            else -> {
                diceThrow + accumulated!!
            }
        }
        return diceThrow
    }


    /**
     * Cambia el jugador que le toca y prepara algunos componentes de la pantalla para jugar la maquina
     */
    fun changePlayer(playerTurn: String?, machine: Boolean, textColor: Int) {
        throwDice.text = resources.getText(R.string.throwDice)
        throwDice.isEnabled = true
        startTurn?.text = resources.getString(R.string.turnToPlay,playerTurn)
        playerToPlay.text = playerTurn
        resetButtons(textColor)
        machineButtonsConf(machine)
    }

    /**
     * prepara algunos componentes de la pantalla para jugar la maquina
     */
    private fun machineButtonsConf(machine: Boolean) {
        if (machine) {
            throwDice.isEnabled = false
            startTurn?.visibility = View.GONE
            throwDice.visibility = View.VISIBLE
            collect.visibility = View.GONE
            animateButtonThrowing()
        }
    }

    /**
     * Empieza la tirada de la maquina
     */
    fun startMachineTurn(restoredInstance: Boolean){
        machineThread = thread(name = resources.getString(R.string.machine), block = {machineTurn(restoredInstance, mCallback!!.machinePoints())})
        if (machineThread != null) {
            machineThread!!.uncaughtExceptionHandler = Thread.UncaughtExceptionHandler { thread, exception -> Log.d(thread.name,exception.printStackTrace().toString()) }
        }
    }

    /**
     * Tirada de la maquina y envio de los datos al hilo principal
     */
    private fun machineTurn(restoredInstance: Boolean, machinePoints: Int) {
        val minThrows = resources.getInteger(R.integer.minThrows)
        val maxThrows = resources.getInteger(R.integer.maxThrows)
        if (!restoredInstance){
            machineThrows = (minThrows..maxThrows).random()
        }
        Thread.sleep(resources.getInteger(R.integer.timerForMachineThrows).toLong()*2)
        val totalThrows= machineThrows
        for (i in 1..totalThrows!!) {
            machineThrows = machineThrows!! - 1
            val diceThrow = throwDice()
            val msg = Message()
            msg.what = resources.getInteger(R.integer.pointConstant)
            val bundle = Bundle()
            bundle.putInt(resources.getString(R.string.point),diceThrow)
            bundle.putInt(resources.getString(R.string.accumulated),accumulated!!)

            msg.data = bundle
            handler.sendMessage(msg)

            if (diceThrow == resources.getInteger(R.integer.autoPassTurn)) {
                return
            }
            if((machinePoints + accumulated!!) >= resources.getInteger(R.integer.maxPoints)){
                return
            }
            Thread.sleep(resources.getInteger(R.integer.timerForMachineThrows).toLong())
        }
        val msg = Message()
        msg.what = resources.getInteger(R.integer.collectConstant)
        val bundle = Bundle()
        bundle.putInt(resources.getString(R.string.accumulated),accumulated!!)
        msg.data = bundle
        handler.sendMessage(msg)
    }

    /**
     * Recepcion de los datos enviados desde las tiradas de la maquina
     */
    private var handler: Handler =
    object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            if(isAdded){
                when (msg.what){
                    resources.getInteger(R.integer.pointConstant) -> {
                        turnAccumulated.text = resources.getString(R.string.turnAccumulated, msg.data.getInt(resources.getString(R.string.accumulated)))
                        mCallback!!.onThrowDice(msg.data.getInt(resources.getString(R.string.point)), accumulated!!)
                    }
                    resources.getInteger(R.integer.collectConstant) -> {
                        accumulated = msg.data.getInt(resources.getString(R.string.accumulated))
                        collectPoints()
                    }
                }
            }
        }
    }

    /**
     * Coloca los botones como al iniciar la app
     */
    private fun resetButtons(textColor: Int) {
        startTurn?.setTextColor(textColor)
        startTurn?.visibility = View.VISIBLE
        throwDice.visibility = View.GONE
        collect.visibility = View.GONE
        if(startTurn == null){
            throwDice.visibility = View.VISIBLE
            collect.visibility = View.INVISIBLE
        }
    }

    /**
     * coloca los botones como al iniciar el turno
     */
    private fun turnStartedButtons() {
        startTurn?.setTextColor(ResourcesCompat.getColor(resources,R.color.player1Color,null))
        startTurn?.visibility = View.GONE
        throwDice.visibility = View.VISIBLE
        collect.visibility = View.INVISIBLE
        if(accumulated!! > resources.getInteger(R.integer.zero)) {
            collect.visibility = View.VISIBLE
        }
    }

    /**
     * Animacion del boton tirar a tirando, se oculta poco a poco y posteriormente se cambia el texto
     * y se muestra poco a poco
     */
    private fun animateButtonThrowing(){

        val animationFadeOut = AnimationUtils.loadAnimation(context,R.anim.fade_out)
        animationFadeOut.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {
            }

            override fun onAnimationEnd(animation: Animation?) {
                throwDice.text = resources.getText(R.string.throwingDice)
                throwDice.animation = AnimationUtils.loadAnimation(context,R.anim.fade_in)
            }

            override fun onAnimationStart(animation: Animation?) {
            }
        })
        throwDice.animation = animationFadeOut
    }

    /**
     * se crea una interfaz que implementa la activity para recibir los datos
     * https://developer.android.com/training/basics/fragments/communicating
     */
    interface ActionPointsTurnCommunicationInterface {
        fun onThrowDice(throwPoints: Int, accumulated: Int)
        fun collectPoints(accumulated: Int)
        fun machinePoints(): Int
    }

}


