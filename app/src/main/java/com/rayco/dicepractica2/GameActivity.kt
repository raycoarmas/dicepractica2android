package com.rayco.dicepractica2

import android.content.res.Configuration
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AlertDialog.*
import android.support.v7.app.AppCompatActivity
import android.support.constraint.ConstraintSet
import android.support.transition.TransitionManager
import android.support.v4.content.res.ResourcesCompat

class GameActivity : AppCompatActivity(), ActionPointsTurnFragment.ActionPointsTurnCommunicationInterface {

    private var playerTurn: String? = null
    private var players: String? = null
    private var dialog: AlertDialog? = null
    private val initialConstraintSet = ConstraintSet()
    private val changedConstraintSet = ConstraintSet()

    /**
     * Inicializa las variables necesarias, crea los framents si no se han creado anteriormente
     * Prepara las contraint para mover los fragments
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        playerTurn = resources.getString(R.string.player1)
        players = intent.getStringExtra(resources.getString(R.string.players))

        if (supportFragmentManager.findFragmentByTag(resources.getString(R.string.PlayerPointsTag))==null) {
            supportFragmentManager.beginTransaction()
                .replace(
                    R.id.playerPointsFragment,
                    PlayerPointsFragment::class.java.newInstance(),
                    resources.getString(R.string.PlayerPointsTag)
                )
                .replace(
                    R.id.actionPointsTurn,
                    ActionPointsTurnFragment::class.java.newInstance(),
                    resources.getString(R.string.ActionPointsTag)
                )
                .replace(
                    R.id.diceFragment,
                    DiceFragment::class.java.newInstance(),
                    resources.getString(R.string.DiceFragmentTag)
                )
                .addToBackStack(null)
                .commit()
        }

        //Se obtiene la imagen actual del layout
        initialConstraintSet.clone(this, R.layout.activity_game)
        changedConstraintSet.clone(this, R.layout.activity_game)

        //Se elimina la cadena
        changedConstraintSet.removeFromHorizontalChain(R.id.actionPointsTurn)
        changedConstraintSet.removeFromHorizontalChain(R.id.diceFragment)

        //Se crean las contratints para mover el dado a la izquierda
        changedConstraintSet.connect(R.id.diceFragment, ConstraintSet.START, R.id.activityGame, ConstraintSet.START,8)
        changedConstraintSet.connect(R.id.diceFragment, ConstraintSet.END, R.id.actionPointsTurn, ConstraintSet.START)
        changedConstraintSet.setMargin(R.id.diceFragment, ConstraintSet.END, 0)

        //Se crean las contratints para mover los botones a la derecha
        changedConstraintSet.connect(R.id.actionPointsTurn, ConstraintSet.END, R.id.activityGame, ConstraintSet.END,8)
        changedConstraintSet.connect(R.id.actionPointsTurn, ConstraintSet.START, R.id.diceFragment, ConstraintSet.END)
        changedConstraintSet.setMargin(R.id.actionPointsTurn, ConstraintSet.START, 0)

        //Se crea la cadena con los 2 elementos
        changedConstraintSet.createHorizontalChain(R.id.diceFragment, ConstraintSet.LEFT,
            R.id.actionPointsTurn, ConstraintSet.RIGHT,
            intArrayOf(R.id.diceFragment,R.id.actionPointsTurn), null, ConstraintSet.CHAIN_SPREAD)
    }

    /**
     * Guarda el estado actual de los atributos
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(resources.getString(R.string.players), players)
        outState.putString(resources.getString(R.string.playerTurn), playerTurn)
    }

    /**
     * Recupera el estado de los atributos numero de jugadores y jugador que le toca
     */
    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        players = savedInstanceState?.getString(resources.getString(R.string.players))
        playerTurn = savedInstanceState?.getString(resources.getString(R.string.playerTurn))
        changeFragmentPositions()
        val playerPointsFragment: PlayerPointsFragment = supportFragmentManager.findFragmentByTag(resources.getString(R.string.PlayerPointsTag)) as PlayerPointsFragment
        if(playerPointsFragment.hasWinner()){
            openDialogEndGame(playerPointsFragment.winner())
        }

    }

    override fun onBackPressed() {
        openDialogBack()
    }

    /**
     * recibe el valor generado para una tirada desde el fragment,
     * cambia la imagen del dado y controla el cambio de jugador y la recoleccion automatica cuando se supera la puntuacion maxima
     */
    override fun onThrowDice(throwPoints: Int, accumulated: Int) {

        val diceFragment: DiceFragment = supportFragmentManager.findFragmentByTag(resources.getString(R.string.DiceFragmentTag)) as DiceFragment
        diceFragment.changeDice(throwPoints)

        val playerPointsFragment: PlayerPointsFragment = supportFragmentManager.findFragmentByTag(resources.getString(R.string.PlayerPointsTag)) as PlayerPointsFragment
        val actionPointsTurnFragment: ActionPointsTurnFragment = supportFragmentManager.findFragmentByTag(resources.getString(R.string.ActionPointsTag)) as ActionPointsTurnFragment

        if(throwPoints == resources.getInteger(R.integer.autoPassTurn)){
            changePlayerTurn()
        }else if ((accumulated + playerPointsFragment.getPlayerPoints(playerTurn)) >= resources.getInteger(R.integer.maxPoints)){
            val winner = playerTurn
            actionPointsTurnFragment.collectPoints()
            openDialogEndGame(winner)
        }
    }

    /**
     * Dialog que aparece al tener un ganador, te permite salir del todo o volver a jugar
     */
    private fun openDialogEndGame(winner: String?) {
        val builder = Builder(this)//Theme_Material_Light_Dialog_Alert
        builder.setMessage(resources.getString(R.string.won, winner))
        builder.setTitle(R.string.gameEnd)
        builder.setPositiveButton(R.string.playAgain) { dialog, _ ->
            val playerPointsFragment: PlayerPointsFragment = supportFragmentManager.findFragmentByTag(resources.getString(R.string.PlayerPointsTag)) as PlayerPointsFragment
            playerPointsFragment.resetPoints()
            dialog.dismiss()
            if(playerTurn == resources.getString(R.string.machine)){
                val actionPointsTurnFragment: ActionPointsTurnFragment = supportFragmentManager.findFragmentByTag(resources.getString(R.string.ActionPointsTag)) as ActionPointsTurnFragment
                actionPointsTurnFragment.startMachineTurn(false)
            }
        }
        builder.setNegativeButton(R.string.exit) { _, _ ->
            finishAffinity()
        }
        builder.setCancelable(false)
        dialog = builder.create()
        dialog!!.show()
    }

    /**
     * Dialog que aparece al pulsar el boton volver
     */
    private fun openDialogBack(){
        val builder = Builder(this)//Theme_Material_Light_Dialog_Alert
        builder.setMessage(resources.getString(R.string.chooseAction))
        builder.setPositiveButton(R.string.resetGame) { dialog, _ ->
            supportFragmentManager.beginTransaction()
                .replace(R.id.playerPointsFragment, PlayerPointsFragment::class.java.newInstance(), resources.getString(R.string.PlayerPointsTag))
                .replace(R.id.actionPointsTurn, ActionPointsTurnFragment::class.java.newInstance(), resources.getString(R.string.ActionPointsTag))
                .replace(R.id.diceFragment, DiceFragment::class.java.newInstance(), resources.getString(R.string.DiceFragmentTag))
                .addToBackStack(null)
                .commit()
            playerTurn = resources.getString(R.string.player1)
            dialog.dismiss()
        }
        builder.setNegativeButton(R.string.exit) { _, _ ->
            finishAffinity()
        }
        builder.setCancelable(false)
        dialog = builder.create()
        dialog!!.show()
    }

    /**
     * Recibe los puntos a recolectar en el fragment los envia a la acumulada real y cambia el turno
     */
    override fun collectPoints(accumulated: Int) {
        val playerPointsFragment: PlayerPointsFragment = supportFragmentManager.findFragmentByTag(resources.getString(R.string.PlayerPointsTag)) as PlayerPointsFragment
        playerPointsFragment.receivePlayerPoints(accumulated, playerTurn)
        changePlayerTurn()
    }

    /**
     * Cambia el atributo del jugador que le toca tirar, y lo comunica al fragment,
     * cambia los fragment cuando la orientacion es LANDSCAPE
     * En caso de jugar la maquina lanza el hilo si no hay ganador aun
     */
    private fun changePlayerTurn(){
        var isMachine = false
        val actionPointsTurnFragment: ActionPointsTurnFragment = supportFragmentManager.findFragmentByTag(resources.getString(R.string.ActionPointsTag)) as ActionPointsTurnFragment
        val playerPointsFragment: PlayerPointsFragment = supportFragmentManager.findFragmentByTag(resources.getString(R.string.PlayerPointsTag)) as PlayerPointsFragment
        var color = ResourcesCompat.getColor(resources,R.color.player1Color,null)
        if (playerTurn.equals(resources.getString(R.string.player1))){
            when (players) {
                resources.getString(R.string.singleplayer) -> {
                    playerTurn = resources.getString(R.string.machine)
                    isMachine = true
                }
                resources.getString(R.string.multiplayer) -> {
                    playerTurn = resources.getString(R.string.player2)

                }
            }
            color = ResourcesCompat.getColor(resources,R.color.player2Color,null)
        }
        else if (playerTurn.equals(resources.getString(R.string.player2)) || playerTurn.equals(resources.getString(R.string.machine))) {
            playerTurn = resources.getString(R.string.player1)
        }
        changeFragmentPositions()
        actionPointsTurnFragment.changePlayer(playerTurn, isMachine, color)

        if(isMachine && !playerPointsFragment.hasWinner()){
            actionPointsTurnFragment.startMachineTurn(false)
        }
    }

    /**
     * Cambia los elementos de la vista cuando esta en landScape, al cambiar de jugador, se intercambia los botones
     * con el dado
     */
    private fun changeFragmentPositions(){
        if(resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE){
            when (playerTurn) {
                resources.getString(R.string.player1) ->{
                    TransitionManager.beginDelayedTransition(findViewById(R.id.activityGame))
                    initialConstraintSet.applyTo(findViewById(R.id.activityGame))
                }
                else -> {
                    TransitionManager.beginDelayedTransition(findViewById(R.id.activityGame))
                    changedConstraintSet.applyTo(findViewById(R.id.activityGame))
                }
            }
        }
    }

    /**
     * Obtiene lo puntos totales de la maquina
     */
    override fun machinePoints(): Int {
        val playerPointsFragment: PlayerPointsFragment = supportFragmentManager.findFragmentByTag(resources.getString(R.string.PlayerPointsTag)) as PlayerPointsFragment
        return playerPointsFragment.getPlayerPoints(resources.getString(R.string.machine))
    }
}
