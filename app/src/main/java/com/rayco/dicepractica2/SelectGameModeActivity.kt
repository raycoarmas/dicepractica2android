package com.rayco.dicepractica2

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class SelectGameModeActivity : AppCompatActivity(), SelectGameModeFragment.SelectGameModeCommunicationInterface {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_game_mode)
    }

    /**
     * se pasa un extra indicando a la siguiente activity GameActivity que es un solo jugador
     */
    override fun singlePlayer(){
        val intent = Intent(applicationContext, GameActivity::class.java)
        intent.putExtra(resources.getString(R.string.players), resources.getString(R.string.singleplayer))
        startActivity(intent)
    }

    /**
     * se pasa un extra indicando a la siguiente activity GameActivity que es de dos jugador
     */
    override fun multiPlayer(){
        val intent = Intent(applicationContext, GameActivity::class.java)
        intent.putExtra(resources.getString(R.string.players), resources.getString(R.string.multiplayer))
        startActivity(intent)
    }
}
